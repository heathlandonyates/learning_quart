# Learning Quart 

This is code to learn quart async framework 

## Getting started

This is a learning repository for quart and asyncio. If you clone the repository for the first time, you may need to generate a python virtual environment. You can run the following commands: 
`python3 -m venv env` in the terminal at the root directory of the repository. Next, you run the command `source ./env/bin/activate`. Finally you can run `pip install -r requirements.txt` to install the dependencies. 

## Project Struction 

- asyncio_demo.py: This is the demo code that simulates fetching a url with associated delays to demonstrate how async calls work via asyncio


## References

- [Asyncio](https://pgjones.gitlab.io/quart/tutorials/asyncio.html#asyncio)
- [Quart](https://pgjones.gitlab.io/quart/index.html)
- [Quart Tutorials](https://pgjones.gitlab.io/quart/tutorials/quickstart.html)