import asyncio

async def simulated_fetch(url, delay): 
    """
    Sleeps given the delay and then returns the url provided 

    args
    ----
    - url:str - The url that is fed to the method
    - delay:int - The integer to delay in seconds 

    return
    ------
    - formatted literal string with url     
    """

    #Put the method asleep for designated delay in seconds 
    await asyncio.sleep(delay)

    #Simulate a response from url with the print statement and return the url 
    print(f"Fetched {url} after {delay}")
    return f"<html>{url}"


def main(): 
    """
    Provides an "event loop" with IO (input/output) and runs loop until the async function returns results 
    """

    #Start the event loop 
    loop = asyncio.get_event_loop()

    #Results return 
    results = loop.run_until_complete(asyncio.gather(
        simulated_fetch('http://google.com',2),
        simulated_fetch('http://bbc.co.uk',1),
    ))

    #Print the results 
    print(results)


#Python basic main logic 
if __name__ == "__main__": 
    main()